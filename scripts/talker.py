#!/usr/bin/env python

import rospy
import time
from mrs_msgs import srv, msg
from nav_msgs import msg as nav_msg

uav = 'uav1'

curr_pos = (0, 0, 0)

def moveTo(x: int, y: int, z: int):
    service: str = f'/{uav}/control_manager/goto'
    rospy.wait_for_service(service)
    try:
        goto = rospy.ServiceProxy(service, srv.Vec4)
        goal = {"goal": (x, y, z, 0) }
        print(type(goal["goal"]))
        resp1 = goto((x, y, z, 0))
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)

def mycall(foo: nav_msg.Odometry):
    global curr_pos
    curr_pos = (foo.pose.pose.position.x, foo.pose.pose.position.y, foo.pose.pose.position.z)

def start_listener():
    rospy.Subscriber("/uav1/odometry/gps_local_odom", nav_msg.Odometry, mycall)

if __name__ == '__main__':
    try:
        rospy.init_node('listener', anonymous=True)
        start_listener()
        time.sleep(1)
        moveTo(curr_pos[0], curr_pos[1], curr_pos[2] + 20)
    except rospy.ROSInterruptException:
        pass
